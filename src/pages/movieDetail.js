import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

function MovieDetail() {
  const { title } = useParams();
  const movieData = useSelector((state) => state.movieData);
  const movie = movieData.movies.find((x) => x.title === title);
  const imgBaseURL = "https://www.themoviedb.org/t/p/original/";

  return (
    <div className="flex space-x-8 pt-8">
      <img className="min-w-40 w-80" src={`${imgBaseURL}${movie.poster_path}`} alt="" />
      <dl className="space-y-4">
        <div>
          <dt className="sr-only">Title</dt>
          <dd className="font-bold text-gray-900 text-4xl">{movie.title}</dd>
        </div>
        <div className="flex space-x-4">
          <div>
            <dt className="sr-only">Year</dt>
            <dt>{movie.release_date}</dt>
          </div>
          <div className="flex items-center text-sky-500">
            <dt className="sr-only">Star Rating</dt>
            <svg width="16" height="20" fill="currentColor" className="mr-2">
              <path d="M7.05 3.691c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.372 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.539 1.118l-2.8-2.034a1 1 0 00-1.176 0l-2.8 2.034c-.783.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.363-1.118L.98 9.483c-.784-.57-.381-1.81.587-1.81H5.03a1 1 0 00.95-.69L7.05 3.69z" />
            </svg>
            <dt className="text-black">{movie.vote_average}</dt>
          </div>
        </div>
        <div>
          <dt className="font-semibold underline">Description</dt>
          <dd className="text-gray-600">{movie.overview}</dd>
        </div>
      </dl>
    </div>
  );
}

// {
//     imageUrl:
//       "https://image.tmdb.org/t/p/w600_and_h900_bestv2/wfLzocgEa7DDQAJWeorEiDFx9WM.jpg",
//     title: "Slaugherhouse Rulez",
//     rating: "R",
//     year: "2018",
//     starRating: "3.22",
//     genre: "Comedy",
//     runtime: "1.5 jam",
//     description:
//       "Don Wallace, a student at the boarding school Slaughterhouse, faces the arcane rules of the establishment when a new threat emerges and the tenants of the school engage in a bloody battle for survival.",
//   },

export default MovieDetail;
