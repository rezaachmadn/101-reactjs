import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";

import MovieCard from "../components/movieCard";
import { getMovies } from "../actions/fetchMovies";
function Movies() {
  const dispatch = useDispatch();
  const movieData = useSelector((state) => state.movieData);

  useEffect(() => {
    dispatch(getMovies());
  },[dispatch]);

  return (
    <div>
      <div className="grid divide-y-2">
        {movieData.movies.map((movie, index) => {
          return <MovieCard {...movie} key={index} />;
        })}
      </div>
    </div>
  );
}

export default Movies;
