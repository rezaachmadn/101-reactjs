const movieFavorite = (state = [], action) => {
  switch (action.type) {
    case "ADD_FAVORITE_MOVIE":
      const res = [...state, action.payload];
      return res;
    case "REMOVE_FAVORITE_MOVIE":
      const favWithoutMovie = state.filter((movie) => movie.id !== action.payload.id);
      return favWithoutMovie;
    default:
      return state;
  }
};

export default movieFavorite;
