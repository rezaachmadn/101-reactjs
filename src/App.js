import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Navbar from "./components/navbar";
import Favourites from "./pages/favourites";
import Movies from "./pages/movies";
import MovieDetail from "./pages/movieDetail";
import "./App.css";

function App() {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route path="/" element={<Movies/>} />
        <Route path="/favourites" element={<Favourites/>} />
        <Route path="/movie/:title" element={<MovieDetail/>} />
      </Routes>
    </Router>
  );
}

export default App;
