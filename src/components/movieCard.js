import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";

import { addFavorite, removeFavorite } from "../actions/favourite";

function MovieCard(props) {
  const dispatch = useDispatch();
  const movieFavorite = useSelector((state) => state.movieFavorite);
  const imgBaseURL = "https://www.themoviedb.org/t/p/original/";

  return (
    <div className="flex items-start space-x-6 p-6 bg-white">
      <div className="flex-col space-y-4 flex items-center">
        <img
          src={`${imgBaseURL}${props.poster_path}`}
          alt=""
          width="60"
          height="88"
          className="flex-none rounded-md bg-gray-100 border-2 border-indigo-500/50"
        />
        {!movieFavorite.some((movie) => movie.title === props.title) ? (
          <button
            className="btn btn--primary text-xs"
            onClick={() => dispatch(addFavorite(props))}
          >
            Add Favorite
          </button>
        ) : (
          <button
            className="btn btn--secondary text-xs"
            onClick={() => dispatch(removeFavorite(props))}
          >
            Remove Favorite
          </button>
        )}
      </div>
      <div className="min-w-0 relative flex-auto">
        <Link to={`/movie/${props.title}`}>
          <h2 className="font-semibold text-gray-900 truncate pr-20">
            {props.title}
          </h2>
        </Link>
        <dl className="mt-2 flex flex-wrap text-sm leading-6 font-medium">
          <div className="absolute top-0 right-0 flex items-center space-x-1">
            <dt className="text-sky-500">
              <span className="sr-only">Star rating</span>
              <svg width="16" height="20" fill="currentColor">
                <path d="M7.05 3.691c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.372 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.539 1.118l-2.8-2.034a1 1 0 00-1.176 0l-2.8 2.034c-.783.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.363-1.118L.98 9.483c-.784-.57-.381-1.81.587-1.81H5.03a1 1 0 00.95-.69L7.05 3.69z" />
              </svg>
            </dt>
            <dd>{props.vote_average}</dd>
          </div>
          <div className="ml-2">
            <dt className="sr-only">Year</dt>
            <dd>{props.release_date}</dd>
          </div>
          <div className="flex-none w-full mt-2 font-normal">
            <dt className="sr-only">Cast</dt>
            <dd className="text-gray-400">{props.overview}</dd>
          </div>
        </dl>
      </div>
    </div>
  );
}

export default MovieCard;
