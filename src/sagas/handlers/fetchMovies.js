import { call, put, takeEvery, take } from "redux-saga/effects";
import fetchGetMovies from "../requests/fetchMovies";

function* handlerGetMovies() {
  try {
    const movies = yield call(fetchGetMovies);
    yield put({ type: "GET_MOVIES_SUCCESS", movies });
  } catch (error) {
    yield put({ type: "GET_MOVIES_FAILED", message: error.message });
  }
}

/*
Memulai fetchUser setiap kali ada action `GET_MOVIES_REQUESTED`.
takeEvery() berarti dapat melakukan fetching data secara bersamaan.
*/

function* watcherMoviesSaga() {
  yield takeEvery("GET_MOVIES_REQUESTED", handlerGetMovies);
}

export default watcherMoviesSaga;
