export const addFavorite = (movie) => {
    return{
        type: "ADD_FAVORITE_MOVIE",
        payload: movie
    }
}

export const removeFavorite = (movie) => {
    return{
        type: "REMOVE_FAVORITE_MOVIE",
        payload: movie
    }
}