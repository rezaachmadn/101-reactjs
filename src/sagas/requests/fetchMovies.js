const TMDBAPIKEY = "48367485a90367721f562c3532360bb3";
const BASE_LINK = "https://api.themoviedb.org/3"

const fetchGetMovies = () => {
  return fetch(`${BASE_LINK}/discover/movie?page=1&api_key=${TMDBAPIKEY}`, {
    method: "GET",
  })
    .then((response) => response.json())
    .catch((error) => {
      throw error;
    });
};

export default fetchGetMovies;