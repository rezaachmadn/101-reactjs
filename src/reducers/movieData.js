const initialState = {
  movies: [],
  loading: false,
  error: null,
};

const movieData = (state = initialState, action) => {
  switch (action.type) {
    case "GET_MOVIES_REQUESTED":
      return {...state, loading: true};
    case "GET_MOVIES_SUCCESS":
      return {...state, loading: false, movies: action.movies.results};
    case "GET_MOVIES_FAILD":
      return {...state, loading: false, error: action.message};
    default:
      return state;
  }
};

export default movieData;
