### take(pattern)​
Creates an Effect description that instructs the middleware to wait for a specified action 
on the Store. The Generator is suspended until an action that matches pattern is dispatched.

### takeMaybe(pattern)​
Same as take(pattern) but does not automatically terminate the Saga on an END action. 
Instead all Sagas blocked on a take Effect will get the END object.

### takeEvery(pattern, saga, ...args)​
Spawns a saga on each action dispatched to the Store that matches pattern

### takeLatest(pattern, saga, ...args)​
Forks a saga on each action dispatched to the Store that matches pattern. 
And automatically cancels any previous saga task started previously if it's still running.

### takeLeading()
Spawns a saga on each action dispatched to the Store that matches pattern. 
After spawning a task once, it blocks until spawned saga completes and then starts to 
listen for a pattern again.

**Compose bisa lebih dari 1 enhancer, kalo create store cuma bisa 1**
enhancers are high-order functions that take createStore and return a new enhanced version of createStore. Take a look at this sample implementation.
```javascript
    const store = compose(
        applyMiddleware(...middleware),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )(createStore)(reducers);
```
```javascript
    // ERROR
    const store = compose(
        reducers,
        applyMiddleware(...middleware),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );
```