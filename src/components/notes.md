**State** adalah objek yang digunakan untuk menyimpan data di dalam komponen, sedangkan **props** adalah obejek yang digunakan untuk menyimpan data yang diterima dari luar komponen.

Reactjs versi 16 ke bawah.. komponen yang dibuat dengan fungsi tidak akan bisa menggunakan state tapi bisa menerima props.

Pada Reactjs versi 16.8, kita sudah bisa menggunakan state dan component life cycle pada function component berkat State Hook

**Stateful components** adalah komponen yang menggunakan state. => class component
Sedangkan **Stateless** adalah komponen yang tidak menggunakan state. => function component

### Source
[https://www.petanikode.com/reactjs-komponen](https://www.petanikode.com/reactjs-komponen)