import { combineReducers } from "redux";

import movieData from "./movieData";
import movieFavorite from "./movieFavorite";

// Reducer - describe how an action change from one state into other
//         - reducer will check which action is called and store will be modified
const reducers = combineReducers({
  movieData,
  movieFavorite,
});

export default reducers;
