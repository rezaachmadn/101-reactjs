import { useSelector } from "react-redux";

import MovieCard from "../components/movieCard";

function Favourites() {
  const favouriteData = useSelector((state) => state.movieFavorite);

  return (
    <div>
      <div className="grid divide-y-2">
        {favouriteData.map((movie, index) => {
          return <MovieCard {...movie} key={index} />;
        })}
      </div>
    </div>
  );
}

export default Favourites;
